import {NgModule} from '@angular/core';
import {Routes} from '@angular/router';

import {AboutComponentComponent} from './about-component/about-component.component';
import {TableComponentComponent} from './table-component/table-component.component';
import {RouterModule} from '@angular/router';


const appRoutes: Routes = [
  { path: '', component: TableComponentComponent},
  { path: 'home', component: TableComponentComponent },
  { path: 'about', component: AboutComponentComponent }
];

@NgModule ({
 imports: [RouterModule.forRoot(appRoutes)],
 exports: [RouterModule]
})
export class AppRoutingModule {
}
