import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatTableDataSource} from '@angular/material';
import {PopUpComponent} from '../pop-up/pop-up.component';
import {DatashareService} from '../datashare.service';
import {Router} from '@angular/router';
import {MatSort} from '@angular/material';


@Component({
  selector: 'app-table-component',
  templateUrl: './table-component.component.html',
  styleUrls: ['./table-component.component.css']
})


export class TableComponentComponent implements OnInit {

  dataSource: MatTableDataSource<any>;
  displayedColumns: string[] = ['username', 'name', 'surname', 'email', 'role', 'date', 'enabled', 'actions'];
  @ViewChild(MatSort) sort: MatSort;
  searchKey: string;


  // disabled = false;


  constructor(private dialog: MatDialog, private dataShare: DatashareService, private router: Router) { }



  ngOnInit() {
    this.dataShare.getUsers().subscribe(
        list => {
          let array = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val()
          };
        });
        this.dataSource = new MatTableDataSource(array);
        this.dataSource.sort = this.sort;
          // if (this.dataSource.data.length < 3) {
          //   this.disabled = true;
          // }
      }
    );
  }

  applyFilter() {
    this.dataSource.filter = this.searchKey.trim().toLowerCase();
  }

  onDelete($key) {
    if (confirm('Are you sure you want to delete record?')){
      this.dataShare.deleteUser($key);
    }
  }
  openDialog(): void {
    const dialogRef = this.dialog.open(PopUpComponent, {
      width: '650px',
    });
  }
}

