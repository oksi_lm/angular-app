import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import {MatDialogModule} from '@angular/material/dialog';
import { MaterialModule } from './material.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import {ReactiveFormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { AboutComponentComponent } from './about-component/about-component.component';
import { TableComponentComponent } from './table-component/table-component.component';
import { HeaderComponent } from './header/header.component';
import {AppRoutingModule} from './app-routing.module';
import { PopUpComponent } from './pop-up/pop-up.component';
import { environment } from '../environments/environment';
import {DatashareService} from './datashare.service';


@NgModule({
  declarations: [
    AppComponent,
    AboutComponentComponent,
    TableComponentComponent,
    HeaderComponent,
    PopUpComponent

  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    MatDialogModule,
    BrowserAnimationsModule,
    MaterialModule,
    AngularFireDatabaseModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    ReactiveFormsModule

  ],
  providers: [DatashareService],
  bootstrap: [AppComponent],
  entryComponents: [PopUpComponent]
})
export class AppModule { }
