import {Injectable} from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {PopUpComponent} from './pop-up/pop-up.component';
import {MatDialog} from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class DatashareService {

  constructor(private firebase: AngularFireDatabase, private dialog: MatDialog) { }
  userList: AngularFireList<any>;

  form = new FormGroup({
    $key: new FormControl(null),
    username: new FormControl('', Validators.required),
    name: new FormControl('', Validators.required),
    surname: new FormControl('', Validators.required),
    email: new FormControl('', Validators.email),
    role: new FormControl(''),
    date: new FormControl('', Validators.required),
    enabled: new FormControl('')
  });

  getUsers() {
    this.userList = this.firebase.list('users');
    return this.userList.snapshotChanges();
  }

  addUser(user) {
    this.userList.push({
      username: user.username,
      name: user.name,
      surname: user.surname,
      email: user.email,
      role: user.role,
      date: user.date,
      enabled: user.enabled
    });
  }

  populateForm(user) {
    const dialogRef = this.dialog.open(PopUpComponent, {
      width: '650px', })
    this.form.setValue(user);
  }

  updateUsers(user) {
    this.userList.update(user.$key,
      {
        username: user.username,
        name: user.name,
        surname: user.surname,
        email: user.email,
        role: user.role,
        date: user.date,
        enabled: user.enabled
      });
  }

  deleteUser($key: string) {
    this.userList.remove($key);
  }

}
