import {Component, OnInit} from '@angular/core';
// import {NgForm} from '@angular/forms';
import {DatashareService} from '../datashare.service';
import {MatDialog} from '@angular/material';


@Component({
  selector: 'app-pop-up',
  templateUrl: './pop-up.component.html',
  styleUrls: ['./pop-up.component.css']
})
export class PopUpComponent implements OnInit {


  constructor(private dialog: MatDialog, private data: DatashareService,  ) { }
  submited: boolean;
  formControls = this.data.form.controls;

  ngOnInit() {
  }

  onSubmit() {
    this.submited = true;
    if (this.data.form.valid) {
      if (this.data.form.get('$key').value == null) {
        this.data.addUser(this.data.form.value);
        } else {
        this.data.updateUsers(this.data.form.value);
      }
      this.submited = false;
    }
    this.dialog.closeAll();
    this.data.form.reset();
  }

 
}
