// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyA_RMI2vK2p8R8JxburM2NPeRH6g7EMjHA",
    authDomain: "management-book.firebaseapp.com",
    projectId: "management-book",
    storageBucket: "management-book.appspot.com",
    databaseURL: "https://management-book-default-rtdb.firebaseio.com/",
    messagingSenderId: "887229401967",
    appId: "1:887229401967:web:9785bc0d48a814330eb69a",
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
